package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.TestBase;
import pages.HomePage;
import pages.RegistrationPage;
import util.utility;

public class RegistrationPageTest extends TestBase {

	HomePage hp;
	RegistrationPage rp;
	
	public RegistrationPageTest() 
	{
		super();
	}
	
	
	
	@BeforeMethod
	public void setup() 
	{
		initialization();
		hp = new HomePage();
		rp = new RegistrationPage();
		
	}
	
	
	@DataProvider
	public Object[][] getTestData()
	{
		Object[][] data = utility.getData("RegistrationPageTest");
		return data;
	}
	
	
	
	@Test(dataProvider = "getTestData")
	public void verifyRegistration(String full_name,String user_email, String user_pwd, String user_conf_pwd) 
	{
		hp.clickOnRegisterLink();
		rp.register(full_name, user_email, user_pwd, user_conf_pwd);
	}
	
	@AfterMethod
	public void tearDown() 
	{
		driver.close();
	}
	
}
