package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.TestBase;
import pages.HomePage;
import pages.LoginPage;

public class LoginPageTest extends TestBase{
	
	public LoginPage lp;
	public HomePage hp ;
	
	public LoginPageTest() 
	{
		super();
	}
	
	@BeforeMethod 
	public void setup() 
	{
		initialization();
		hp = new HomePage();
		lp = new LoginPage();
	}
	
	@Test
	public void verifyLogin() 
	{ 
		hp.clickOnLoginLink();
		lp.logInAction(prop.getProperty("user_email"), prop.getProperty("user_pwd"));
		Assert.assertEquals(true, false);
		
		
	}
	
	@AfterMethod()
	public void tearDown() 
	{
		driver.close();
	}
	
	
	

}
