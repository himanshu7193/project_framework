package util;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import base.TestBase;

public class Listeners implements ITestListener {

	utility utl = new utility();
	
	public void onTestStart(ITestResult result)
	{
			
	}
 
	public void onTestSuccess(ITestResult result) 
	{
		
	}

	public void onTestFailure(ITestResult result) {
		try 
		{
			utl.screenShot(result.getName());
			System.out.println("Screenshot taken");
		} 
		catch (IOException e) {
		
			e.printStackTrace();
		}
		
		
		
	}

	public void onTestSkipped(ITestResult result) 
	{
			
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) 
	{
		
	}

	public void onStart(ITestContext context) 
	{
		
	}

	public void onFinish(ITestContext context) 
	{
		
	}

}

