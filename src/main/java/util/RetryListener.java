package util;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryListener implements IRetryAnalyzer{

	int counter = 0;
	int maxRetries = 2;
	
	
	public boolean retry(ITestResult result) {
		
		if(counter<maxRetries) 
		{
			counter++;
			return true;
		}
		
		return false;
	}

}
