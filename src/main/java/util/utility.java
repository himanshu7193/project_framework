package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import base.TestBase; 

public class utility extends TestBase {
	
	
	static Workbook book;
	static Sheet sheet;
	public static String TESTDATA_SHEET_PATH = "F:\\New Workspace\\Project\\src\\main\\java\\testData\\TestData.xlsx";

	
	public void screenShot(String testName) throws IOException 
	{
		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("F:\\New Workspace\\Project\\Screenshots\\"+testName +"_screenshot.png"));
	}

	public static Object[][] getData(String sheetName) 
	{
		FileInputStream fis =null;
		try
		{
			fis = new FileInputStream(TESTDATA_SHEET_PATH);
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		try 
		{
			book = new XSSFWorkbook(fis);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		sheet= book.getSheet(sheetName);
		int numOfRows = sheet.getLastRowNum();
		
		Object[][] data = new Object[numOfRows][sheet.getRow(0).getLastCellNum()];
		
		for(int i=0;i<numOfRows;i++) 
		{
			for(int j=0;j<sheet.getRow(0).getLastCellNum();j++) 
			{
				data[i][j] = sheet.getRow(i+1).getCell(j).toString();
			}		
		}
		
		return data;
		
	}
	
	
	
	
}
